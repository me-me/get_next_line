/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 15:46:02 by abnaceur          #+#    #+#             */
/*   Updated: 2016/12/01 15:57:23 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *str, int n, size_t len)
{
	unsigned char *tmp;

	tmp = (unsigned char*)str;
	while (len-- > 0)
		*tmp++ = (unsigned char)n;
	return (str);
}
