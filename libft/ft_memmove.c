/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 17:12:23 by abnaceur          #+#    #+#             */
/*   Updated: 2016/12/01 17:42:22 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	char *tdst;
	char *tsrc;

	tdst = (char*)dst;
	tsrc = (char*)src;
	if (tsrc < tdst)
	{
		tsrc = tsrc + n - 1;
		tdst = tdst + n - 1;
		while (n > 0)
		{
			*tdst-- = *tsrc--;
			n--;
		}
	}
	else
	{
		while (n > 0)
		{
			*tdst++ = *tsrc++;
			n--;
		}
	}
	return (dst);
}
