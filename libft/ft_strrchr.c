/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 14:39:46 by abnaceur          #+#    #+#             */
/*   Updated: 2016/12/12 21:52:31 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char *f;

	f = NULL;
	while (*s != '\0')
	{
		if (*s == (char)c)
			f = (char*)s;
		s++;
	}
	if (*s == (char)c)
		return ((char*)s);
	else
		return (f);
}
