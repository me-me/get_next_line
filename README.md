# Get_next_line #

Get_next_line est un projet individuel à 42 qui lit essentiellement un fichier ligne par ligne.
P.S: il existe tellement de méthodes plus simples pour ce faire en utilisant des fonctions C standard. Mais le but ici est de pouvoir le faire en utilisant toutes les fonctions de nos libft et seulement les fonctions standard read, malloc et free.