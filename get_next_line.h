/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 07:51:02 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/21 07:38:01 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include "libft/libft.h"
# include <stdlib.h>
# include <unistd.h>

/*
** Maros
*/

# define BUFF_SIZE 32
# define F free

/*
** This macros has been made for NORM regulation
*/

# define FREE(t_gnl) {F((*e).content); e->content = NULL; e->content_size = 0;}

/*
** Structure
*/

typedef struct		s_llst
{
	void			*content;
	size_t			content_size;
	struct s_llst	*next;
	int				fd;
}					t_gnl;

/*
** Prototype
*/

int					get_next_line(int const fd, char **line);
int					gnl_maker(int fd, char *buf, t_gnl **head);
t_gnl				*gnl_search(int fd, t_gnl **head);
char				*gnl_retreiver(int fd, t_gnl **head);

#endif
