/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 07:50:42 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/21 07:37:51 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** Project	:	Get Next Line-GNL
** Objective:	Read an input file passed as an argument line by line
**		return 1, -1 or 0, valid line, error, EOF
*/

/*
** The main function GNL
*/

int		get_next_line(int fd, char **line)
{
	char			buf[BUFF_SIZE + 1];
	int				ret;
	int				maker;
	static t_gnl	*head;

	while ((ret = read(fd, buf, BUFF_SIZE)) != 0)
	{
		if (ret < 0 || fd < 0 || line == NULL)
			return (-1);
		buf[ret] = '\0';
		if ((maker = gnl_maker(fd, buf, &head)) == 1)
		{
			*line = gnl_retreiver(fd, &head);
			return (1);
		}
		if (maker == -1)
			return (-1);
	}
	if ((*line = gnl_retreiver(fd, &head)) != NULL)
		return (1);
	return (0);
}

/*
** gnl_retreiver: retreive the line from the buff
*/

char	*gnl_retreiver(int fd, t_gnl **head)
{
	t_gnl	*e;
	char	*bgn;
	char	*end;
	char	*cpy;

	e = gnl_search(fd, head);
	if (e->content == NULL)
		return (NULL);
	if ((end = ft_strstr((char *)e->content, "\n")) != NULL)
	{
		if (!(bgn = ft_strsub((char *)e->content, 0,
						(e->content_size - ft_strlen(++end) - 2))))
			return (NULL);
		cpy = e->content;
		e->content = (ft_strlen(end) == 0) ? NULL : (void *)ft_strdup(end);
		e->content_size = ft_strlen(end) + 1;
		free(cpy);
	}
	else
	{
		if (!(bgn = ft_strdup((char *)e->content)))
			return (NULL);
		FREE(e);
	}
	return (bgn);
}

/*
** gnl search: this function search for the correct FD for handling multiple
** file descriptor within the structure
*/

t_gnl	*gnl_search(int fd, t_gnl **head)
{
	t_gnl	*tmp;

	tmp = *head;
	while (tmp && tmp->fd != fd)
		tmp = tmp->next;
	return (tmp);
}

/*
** gnl maker: get the value of the fd 1, -1 or 0)
*/

int		gnl_maker(int fd, char *buf, t_gnl **head)
{
	t_gnl	*elem;
	char	*str;

	elem = gnl_search(fd, head);
	if (elem == NULL)
	{
		if (!(elem = (t_gnl *)ft_lstnew((void *)buf, (ft_strlen(buf) + 1))))
			return (-1);
		elem->fd = fd;
		ft_lstaddback((t_list **)head, (t_list *)elem);
	}
	else
	{
		if (!(str = ft_strjoin((char *)elem->content, buf)))
			return (-1);
		free(elem->content);
		elem->content = (void *)str;
		elem->content_size = ft_strlen(str) + 1;
	}
	if (ft_strstr((char *)elem->content, "\n") != NULL)
		return (1);
	return (0);
}
